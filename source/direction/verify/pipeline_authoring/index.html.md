---
layout: markdown_page
title: "Category Direction - Pipeline Authoring"
description: "Defining your pipelines is an important part of Continuous Integration, and we want to make it as easy as possible."
canonical_path: "/direction/verify/pipeline_authoring/"
---

- TOC
{:toc}

## Our Mission

Defining your pipelines is an essential step to getting started with Continuous Integration. In the Ops Section [direction](/direction/ops/#operations-for-all) this is the gateway to enable "Operations for All", where our goal is to make the authoring experience as easy as possible for both novice and advanced users alike. We believe that creating a green pipeline quickly and easily will help development teams leverage our CI to increase their efficiency. One of the ways we measure success is by improving the [% of green pipeline](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#verifypipeline-authoring---other----first-green-project).

## Our vision

<iframe width="560" height="315" src="https://www.youtube.com/embed/hInM7JUEH4Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

As a practitioner of [Speedy, Reliable Pipelines](/direction/ops/#speedy-reliable-pipelines), GitLab wants your time to a green pipeline to be the fastest on any platform.

When you're setting up something new, and especially when learning a new CI platform, there can be a lot to take in, and it can be hard to even know what you don't know, and what kinds of options and strategies are available to you. This is why our focus over the next three years is to create an amazing authoring experience in a way that leads to getting your first green pipeline as quickly as possible while leveraging all the available features and functionalities GitLab CI can offer.

### Our top vision items are:

  - Improving the linter to help you troubleshoot your pipeline configuration in case of an error by [Identifing which CI YAML triggered a warning or an error](https://gitlab.com/gitlab-org/gitlab/-/issues/273370)
  - Support you as you author your pipeline by [auto-generating a json schema](https://gitlab.com/gitlab-org/gitlab/-/issues/299190) which we can contribute upstream and can be used by any external editor (VScode, sublime text, etc...)
 - Templates are an easy way to get yourself familiar with CI this is why we want to improve our CI templating experience and to explore some [visionary mockups](https://gitlab.com/groups/gitlab-org/-/epics/6022). Our goal is to help you quickly set up pipelines and get to that first green pipeline. 
  - Adding a [pipeline visual builder](https://gitlab.com/groups/gitlab-org/-/epics/4499) to make it easy to see the options that are available to you what the pipeline will actually look like when it runs. 

## Who we are focusing on? 

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in depth look at the our target personas across Ops. For Pipeline Authoring, our "What's Next & Why" are targeting the following personas, as ranked by priority for support: 

1. [Priyanka - Platform engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
1. [Devon - DevOps Engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
1. [Sasha - Software Developer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)

### Everyone can contribute 

If you have any feedback on our 3 year vision which you would like to share please do so in the [Pipeline authoring 3 year vision](https://gitlab.com/groups/gitlab-org/-/epics/4534)



### Additional Resources

For more general information on CI direction see also the general [Continuous Integration category direction](/direction/verify/continuous_integration/). You may also be looking for one of the following related product direction pages: [GitLab Runner](/direction/verify/runner/), [Continuous Delivery](/direction/release/continuous_delivery/), [Release stage](/direction/ops/#release), or [Jenkins Importer](/direction/verify/jenkins_importer/).

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3APipeline%20Authoring)
- [Topic Labels](https://gitlab.com/groups/gitlab-org/-/labels?utf8=%E2%9C%93&subscribed=&search=ci%3A%3A) (because Pipeline Authoring is such a large category, it is broken out into topic areas)
- [Overall Vision](/direction/ops/#verify)

## What's Next & Why

* The work on 14.0 scoped, and you can see the assigned issues on our [pipeline authoring 14.0 planning issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/15) . In this milestone, we would be focusing on technical debt and breaking changes, in addition, we are working on several features notably:

* [Pipeline Editor - Branch selector](https://gitlab.com/groups/gitlab-org/-/epics/5393) - to support users that are working from a non-default branch
* [Pipeline drawer - prepopulate with three stage pipeline](https://gitlab.com/gitlab-org/gitlab/-/issues/300501) - to help you get started with GitLab CI 

The planning for version 14.1 is undergoing, you can view and contribute directly to the [planning issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/21)

## Maturity Plan

We are currently working to mature the Pipeline authoring category from minimal to viable. Definitions of these maturity levels can be found on GitLab's [Maturity page](https://about.gitlab.com/direction/maturity/). The following epics group the functionality we have planned to mature pipeline authoring.

* [Pipeline Editor - MVC](https://gitlab.com/groups/gitlab-org/-/epics/4540)
* [Pipeline Editor - Branch selector](https://gitlab.com/groups/gitlab-org/-/epics/5393)
* [Pipeline Editor - 1st time expirence](https://gitlab.com/groups/gitlab-org/-/epics/5276)

## Competitive Landscape

Our main competitor doing innovative things in terms of pipeline authoring is GitHub, who have evolved Actions into more and more of a standalone CI/CD solution. GitLab remains far ahead in a lot of areas of CI/CD that they are going to have to catch up on, but Microsoft and GitHub have a lot of resources and have a large user base excited to use their product, especially when given away as part of a package. Actions has a more event-driven and community plugin-based approach than GitLab, whereas we take community contributions directly into the product where they can be maintained.

GitHub actions are a seemingly powerful toolkit with a high potential for low maintainability with community contributions as we have seen with Jenkins. We are monitoring to swiftly incorporate the best of their innovation into our product. We've already had some successes [running GitHub Actions directly in GitLab CI](https://gitlab.com/snippets/1988376) and will continue to explore that. We are also working on a [migration guide](https://gitlab.com/gitlab-org/gitlab/-/issues/228937) to help teams we're hearing from who are looking to move over to GitLab have an easier time. Features like [making the script section in containers optional](https://gitlab.com/gitlab-org/gitlab/-/issues/223203) would make it easier to build reusable plugins within GitLab that would behave more like Actions and [Allow `needs:` (DAG) to refer to a job in the same stage](https://gitlab.com/gitlab-org/gitlab/-/issues/30632) to enable users to run an entire pipline without defining stages. 

A limitation of the [GitHub Actions API](https://docs.github.com/en/rest/reference/actions) is the exclusiveness interaction with the service via a workflow YAML checked into a repository. By contrast, GitLab enables users to define units of work to execute as a service, for example, via [mutli-project pipelines](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html), [dynamic child pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html#dynamic-child-pipelines) and [parent-child pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html).

 Watch this walkthrough video of Github actions 
 
 <figure class="video_container">
  <iframe src="https://www.youtube.com/embed/OlgXHnCPZZs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Top Customer Issue(s)

Our top customer issues ([search](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3APipeline+Authoring&label_name%5B%5D=customer&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93)) include the following:


- [Allow `needs:` (DAG) to refer to a job in the same stage](https://gitlab.com/gitlab-org/gitlab/-/issues/30632)



## Top Internal Customer Issue(s)

Our top internal customer issues ([search](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3APipeline%20Authoring&label_name[]=internal%20customer)) include the following:

Our top dogfooding issues ([search](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3APipeline%20Authoring&label_name[]=Dogfooding)) are:

- [Use artifact relative URLs to fetch Knapsack and Flaky tests metadata](https://gitlab.com/gitlab-org/gitlab/-/issues/32222)
- [Add syntax for importing a job that is "virtual" so it is not run by default](https://gitlab.com/gitlab-org/gitlab/-/issues/31304)

## Analyst Landscape

Pipeline Authoring is not independently analyzed as an analyst category. See our [Continuous Integration Direction](/direction/verify/continuous_integration/) for this content.


