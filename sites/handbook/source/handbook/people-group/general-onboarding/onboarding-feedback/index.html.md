---
layout: handbook-page-toc
title: "GitLab Onboarding Feedback"
description: "Onboarding Feedback"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Onboarding Feedback 

Wondering if you are doing okay with your onboarding? Need some suggestions or guidance? Then this is the right page, let's hear from team members who share their onboarding experience and any helpful tips! 

### Eduardo Guillen

<!-- blank line -->
<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/E0Lj_JhXzy0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
<!-- blank line -->

### {Insert team member name}

- Embed the video created once uploaded to GitLab Unfiltered YouTube Channel


## Team Member Video Process

Did you have a blast during your onboarding? Or just want to share some helpful tips that you learned during your onboarding? Then why not share this with our new team members. Simply create a short video of yourself! 

### Things to possibly include:

- General onboarding experience (from the tasks, to the people, to your feelings)
- What went well
- What didn't go as well (if you want to)
- General tips to any new team members watching the video's
- Really anything you want to mention about the onboarding experience

### Timing

It's great that you are wanting to share your experience, anything up to 10 minutes should be good! Keeping it short and sweet is always best to keep your audience’s attention :)  

### Notes

- All video's shared will be uploaded to the GitLab Unfiltered Channel and to this Handbook page. 
- Once you have recorded your video, please reach out to the People Experience team in the [#peopleops Slack channel](https://gitlab.slack.com/archives/C0SNC8F2N) or by email: `people-exp @gitlab.com`. 
