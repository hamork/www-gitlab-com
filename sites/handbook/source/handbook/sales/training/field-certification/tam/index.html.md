---
layout: handbook-page-toc
title: "Field Certification: Technical Account Managers"
description: "To support and scale GitLab’s continued growth and success, the Field Enablement Team is developing a certification program for Technical Account Managers that includes functional, soft skills, and technical training for field team members"
---

## Field Certification Program for Technical Account Managers 
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview 
To support and scale GitLab’s continued growth and success, the Field Enablement Team is developing a certification program for Technical Account Managers that includes functional, soft skills, and technical training for field team members.  

Certification badges will align to the critical customer engagements as well as the [field functional competencies](/handbook/sales/training/field-functional-competencies/) that address the critical knowledge, skills, role-based behaviors, processes, content, and tools to successfully execute each customer interaction.

## Technical Account Managers Curriculum 
The below slide shows the holistic learner journey for TAMs and provides context for the information included throughout the learning program. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQvjB6E9JlplzwqBHVv2fFGAEGZwqjg4AZQO-p_DqjX7znjZGOC_q2-d2xCbwr2LbfXCmyOvVxcirYb/embed?start=false&loop=false&delayms=3000&slide=id.g94bb3b04a3_0_492" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## Upcoming courses for TAMs
It is an exciting quarter for TAM enablement. The learning objectives for each course are outlined below.

**Course 1: [Building Success Plans](https://gitlab.edcast.com/pathways/ECL-47775d76-4bc6-4fe8-bdde-07f6ffc56578)**
* Define success plans and their purpose
* Distinguish between success plans and collaboration projects
* Explain the process of creating a success plan
* Define well-written customer outcomes

**Course 2: Success Plans in Gainsight**
* How to build success plan in gainsight
* What to put into fields like Strategy
* Building objectives and related tasks in Gainsight 

**Course 3: Executive Business Reviews**
* Define what is an EBR
* Describe why we do EBRs
* List who should be at EBR, and their role
* Utilize content from success plan to build EBR Deck
* Demonstrate the value of an EBR to customers (to assist in getting EBR scheduled)
* Facilitate scheduling EBRs in a timely fashion with execs and economic buyers.

**Course 4: SALSA-TAM Meeting**
* Identify roles & responsibilities in the SALSA-TAM Meeting
* Review SALSA-TAM Meeting best practices
* Leverage tools during call 

**Course 5: Pre- to Post Sales Handoff**
*Differentiate between when a TAM should get involved in commercial and enterprise accounts
* Review the Customer Use Case with SAL and SA
* Analyze the Customer’s Background
* Identify gaps in understanding for planning phase

**Course 6: GitLab Days**
 Learning Objectives are TBD 


 


# Recognition
Upon completing each course, the associate will receive a badge. 

# Feedback 
We want to hear from you! You can follow along with course development by checking out the issues on the [Field Cert Team Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1637426?&label_name[]=field%20certification). 

